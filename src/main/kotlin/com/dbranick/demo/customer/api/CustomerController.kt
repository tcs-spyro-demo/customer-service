package com.dbranick.demo.customer.api

import com.dbranick.demo.customer.application.CustomerFacade
import com.dbranick.demo.customer.application.CustomerOrderFacade
import com.dbranick.demo.customer.application.CustomerOrderRestDTO
import com.dbranick.demo.customer.application.CustomerRestDTO
import io.swagger.annotations.Api
import io.swagger.annotations.ApiOperation
import io.swagger.annotations.ApiParam
import org.springframework.http.HttpStatus
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.*

@Api("Customer")
@RestController
internal class CustomerController(
	private val customerFacade: CustomerFacade,
	private val customerOrderFacade: CustomerOrderFacade
) {

	@ApiOperation("Create customer")
	@PutMapping("/customers")
	fun createCustomer(
		@ApiParam("Command", required = true)
		@RequestBody command: CreateCustomerRestCommand
	): ResponseEntity<String> {
		return ResponseEntity(customerFacade.createCustomer(command), HttpStatus.CREATED)
	}

	@ApiOperation("Get customer")
	@GetMapping("/customers/{id}")
	fun getCustomer(
		@ApiParam("Customer ID", required = true)
		@PathVariable id: String
	): CustomerRestDTO {
		return customerFacade.getCustomer(id)
	}

	@ApiOperation("Get customer orders")
	@GetMapping("/customers/{id}/orders")
	fun getCustomerOrders(
		@ApiParam("Customer ID", required = true)
		@PathVariable id: String
	): List<CustomerOrderRestDTO> {
		return customerOrderFacade.getCustomerOrders(id)
	}
}