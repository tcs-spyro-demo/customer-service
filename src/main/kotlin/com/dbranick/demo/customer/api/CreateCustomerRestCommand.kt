package com.dbranick.demo.customer.api

import com.dbranick.demo.customer.application.CreateCustomerCommand

internal data class CreateCustomerRestCommand(
	override val firstName: String,
	override val lastName: String,
	override val email: String,
): CreateCustomerCommand