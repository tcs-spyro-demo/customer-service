package com.dbranick.demo.customer.domain

internal interface CustomerRepository {

	fun save(customer: Customer)
	fun findCustomer(customerId: CustomerId): Customer?
}