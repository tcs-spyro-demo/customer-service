package com.dbranick.demo.customer.domain

import java.util.*

internal class CustomerOrderId(val value: String) {
	constructor() : this(UUID.randomUUID().toString())
}