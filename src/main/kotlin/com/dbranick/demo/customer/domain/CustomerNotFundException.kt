package com.dbranick.demo.customer.domain

internal class CustomerNotFundException(customerId: CustomerId) : Exception("Customer not found ${customerId.value}")