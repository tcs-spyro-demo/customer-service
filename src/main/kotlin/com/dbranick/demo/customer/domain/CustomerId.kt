package com.dbranick.demo.customer.domain

import java.util.*

internal data class CustomerId(val value: String) {
	constructor() : this(UUID.randomUUID().toString())
}
