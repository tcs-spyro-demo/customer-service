package com.dbranick.demo.customer.domain

import org.springframework.data.mongodb.core.mapping.Document
import java.time.LocalDateTime

@Document("customer_orders")
internal class CustomerOrder(
	val orderId: String,
	val customerId: CustomerId,
	val number: String,
	val createDate: LocalDateTime,
	var lastModificationDate: LocalDateTime,
	var status: CustomerOrderStatus = CustomerOrderStatus.NEW
) {
	var id: CustomerOrderId = CustomerOrderId()
		private set
}


internal enum class CustomerOrderStatus {
	NEW,
	PAID,
	READY,
	CONFIRMED,
	DELIVERED
}