package com.dbranick.demo.customer.domain

internal interface CustomerOrderRepository {
	fun save(customerOrder: CustomerOrder)
	fun findByOrderId(orderId: String): CustomerOrder?
	fun getAllCustomerOrders(customerId: CustomerId): List<CustomerOrder>
}