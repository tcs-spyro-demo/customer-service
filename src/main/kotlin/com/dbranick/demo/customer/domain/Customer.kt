package com.dbranick.demo.customer.domain

import org.springframework.data.annotation.Id
import org.springframework.data.mongodb.core.mapping.Document

@Document("customers")
internal class Customer(
	val firstName: String,
	val lastName: String,
	val email: String
) {
	@Id
	var id: CustomerId = CustomerId()
		private set
}