package com.dbranick.demo.customer.infrastructure.db

import com.dbranick.demo.customer.domain.CustomerId
import com.dbranick.demo.customer.domain.CustomerOrder
import com.dbranick.demo.customer.domain.CustomerOrderId
import com.dbranick.demo.customer.domain.CustomerOrderRepository
import org.springframework.data.mongodb.core.MongoTemplate
import org.springframework.data.mongodb.core.find
import org.springframework.data.mongodb.core.findById
import org.springframework.data.mongodb.core.findOne
import org.springframework.data.mongodb.core.query.Criteria
import org.springframework.data.mongodb.core.query.Query.query
import org.springframework.stereotype.Component

@Component
internal class MongoCustomerOrderRepository(
	private val mongoTemplate: MongoTemplate
) : CustomerOrderRepository {
	override fun save(customerOrder: CustomerOrder) {
		mongoTemplate.save(customerOrder)
	}

	override fun findByOrderId(orderId: String): CustomerOrder? {
		val query = query(Criteria.where("orderId").`is`(orderId))
		return mongoTemplate.findOne(query)
	}

	override fun getAllCustomerOrders(customerId: CustomerId): List<CustomerOrder> {
		val query = query(Criteria.where("customerId").`is`(customerId))
		return mongoTemplate.find(query)
	}
}