package com.dbranick.demo.customer.infrastructure.configuration

import com.fasterxml.jackson.databind.ObjectMapper
import com.fasterxml.jackson.databind.SerializationFeature
import com.fasterxml.jackson.databind.util.StdDateFormat
import com.fasterxml.jackson.datatype.jdk8.Jdk8Module
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule
import com.fasterxml.jackson.module.kotlin.KotlinModule
import com.fasterxml.jackson.module.paramnames.ParameterNamesModule
import org.springframework.amqp.core.Queue
import org.springframework.amqp.rabbit.connection.CachingConnectionFactory
import org.springframework.amqp.rabbit.connection.ConnectionFactory
import org.springframework.amqp.rabbit.core.RabbitTemplate
import org.springframework.amqp.support.converter.Jackson2JsonMessageConverter
import org.springframework.boot.context.properties.ConfigurationProperties
import org.springframework.boot.context.properties.ConstructorBinding
import org.springframework.boot.context.properties.EnableConfigurationProperties
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration
import org.springframework.messaging.converter.MappingJackson2MessageConverter


@Configuration
@EnableConfigurationProperties(RabbitConfig::class)
internal class RabbitConfiguration {

	@Bean
	fun connectionFactory(rabbitConfig: RabbitConfig): ConnectionFactory = CachingConnectionFactory()
		.apply {
			host = rabbitConfig.host
			port = rabbitConfig.port
			username = rabbitConfig.username
			setPassword(rabbitConfig.password)
		}

	@Bean
	fun jackson2JsonMessageConverter(): Jackson2JsonMessageConverter {
		return Jackson2JsonMessageConverter(objectMapper())
	}

	@Bean
	fun consumerJackson2MessageConverter(): MappingJackson2MessageConverter {
		val mappingJackson2MessageConverter = MappingJackson2MessageConverter()
		mappingJackson2MessageConverter.objectMapper = objectMapper()
		return mappingJackson2MessageConverter
	}

	@Bean
	fun orderQueue() = Queue("orderQueue")

	private fun objectMapper(): ObjectMapper {
		val mapper = ObjectMapper()
			.registerModule(ParameterNamesModule())
			.registerModule(Jdk8Module())
			.registerModule(JavaTimeModule())
			.registerModule(KotlinModule())
		mapper.disable(SerializationFeature.WRITE_DATES_AS_TIMESTAMPS)
		mapper.dateFormat = StdDateFormat()
		return mapper
	}
}

@ConstructorBinding
@ConfigurationProperties(prefix = "demo.rabbit")
internal data class RabbitConfig(
	val host: String = "localhost",
	val port: Int = 5672,
	val username: String = "guest",
	val password: String = "guest"
)