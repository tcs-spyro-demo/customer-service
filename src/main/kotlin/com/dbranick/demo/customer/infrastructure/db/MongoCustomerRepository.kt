package com.dbranick.demo.customer.infrastructure.db

import com.dbranick.demo.customer.domain.Customer
import com.dbranick.demo.customer.domain.CustomerId
import com.dbranick.demo.customer.domain.CustomerRepository
import org.springframework.data.mongodb.core.MongoTemplate
import org.springframework.data.mongodb.core.findById
import org.springframework.stereotype.Component

@Component
internal class MongoCustomerRepository(
	private val mongoTemplate: MongoTemplate
) : CustomerRepository {
	override fun save(customer: Customer) {
		mongoTemplate.save(customer)
	}

	override fun findCustomer(customerId: CustomerId): Customer? {
		return mongoTemplate.findById(customerId)
	}
}