package com.dbranick.demo.customer.infrastructure.configuration

import com.mongodb.ConnectionString
import com.mongodb.MongoClientSettings
import com.mongodb.client.MongoClient
import com.mongodb.client.MongoClients
import org.springframework.boot.context.properties.ConfigurationProperties
import org.springframework.boot.context.properties.ConstructorBinding
import org.springframework.boot.context.properties.EnableConfigurationProperties
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration
import org.springframework.data.mongodb.core.MongoTemplate
import org.springframework.data.mongodb.MongoTransactionManager

import org.springframework.data.mongodb.MongoDatabaseFactory



@Configuration
@EnableConfigurationProperties(DatabaseConfig::class)
internal class DatabaseConfiguration(
	private val databaseConfig: DatabaseConfig
) {

	@Bean
	fun mongoClient(): MongoClient {
		val connectionString = ConnectionString(
			"mongodb://${databaseConfig.username}:" +
				"${databaseConfig.password}@localhost:" +
				"${databaseConfig.port}/${databaseConfig.databaseName}"
		)
		val mongoClientSettings = MongoClientSettings.builder()
			.applyConnectionString(connectionString)
			.build()

		return MongoClients.create(mongoClientSettings)
	}

	@Bean
	fun mongoTemplate(mongoClient: MongoClient): MongoTemplate {
		return MongoTemplate(mongoClient, databaseConfig.databaseName)
	}

	@Bean
	fun transactionManager(dbFactory: MongoDatabaseFactory): MongoTransactionManager {
		return MongoTransactionManager(dbFactory)
	}

}

@ConstructorBinding
@ConfigurationProperties(prefix = "demo.database")
internal data class DatabaseConfig(
	val password: String = "demopass",
	val username: String = "demouser",
	val databaseName: String = "demo",
	val port: Int = 27017,
)