package com.dbranick.demo.customer

import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.runApplication

@SpringBootApplication
public class CustomerApplication

public fun main(args: Array<String>) {
	runApplication<CustomerApplication>(*args)
}
