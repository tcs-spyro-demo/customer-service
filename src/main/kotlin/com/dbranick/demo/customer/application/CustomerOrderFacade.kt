package com.dbranick.demo.customer.application

import com.dbranick.demo.customer.domain.CustomerId
import com.dbranick.demo.customer.domain.CustomerOrder
import com.dbranick.demo.customer.domain.CustomerOrderRepository
import com.dbranick.demo.customer.domain.CustomerOrderStatus
import org.springframework.stereotype.Component
import org.springframework.transaction.annotation.Transactional
import java.time.LocalDateTime

@Component
internal class CustomerOrderFacade(
	private val customerOrderRepository: CustomerOrderRepository
) {

	fun createCustomerOrder(
		customerId: CustomerId,
		orderId: String,
		orderNumber: String,
		createDate: LocalDateTime,
		lastModificationDate: LocalDateTime
	) {
		val customerOrder = CustomerOrder(orderId, customerId, orderNumber, createDate, lastModificationDate)
		customerOrderRepository.save(customerOrder)
	}

	@Transactional
	fun updateCustomerOrderStatus(orderId: String, orderStatus: CustomerOrderStatus, lastModificationDate: LocalDateTime) {
		val customerOrder = customerOrderRepository.findByOrderId(orderId)
			?: error("Customer order not found")

		customerOrder.lastModificationDate = lastModificationDate
		customerOrder.status = orderStatus

		customerOrderRepository.save(customerOrder)
	}

	fun getCustomerOrders(id: String): List<CustomerOrderRestDTO> {
		return customerOrderRepository.getAllCustomerOrders(CustomerId(id))
			.map {
				CustomerOrderRestDTO(
					orderId = it.orderId,
					number = it.number,
					lastModificationDate = it.lastModificationDate,
					createDate = it.createDate,
					status = it.status.name
				)
			}

	}
}