package com.dbranick.demo.customer.application

import java.time.LocalDateTime

internal class CustomerOrderRestDTO(
	val orderId: String,
	val number: String,
	val createDate: LocalDateTime,
	val lastModificationDate: LocalDateTime,
	val status: String
)