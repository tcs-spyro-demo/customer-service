package com.dbranick.demo.customer.application

import com.dbranick.demo.customer.domain.CustomerId
import com.dbranick.demo.customer.domain.CustomerOrderStatus
import com.dbranick.demo.customer.logger
import org.springframework.amqp.rabbit.annotation.RabbitListener
import org.springframework.stereotype.Component

@Component
internal class OrderEventListener(
	private val customerOrderFacade: CustomerOrderFacade
) {

	@RabbitListener(queues = ["orderQueue"])
	fun handleOnOrderCreated(event: OrderCreated) {
		logger.info("Handled event $event")
		with(event) {
			customerOrderFacade.createCustomerOrder(
				customerId = CustomerId(customerId),
				orderId = orderId,
				orderNumber = number,
				createDate = lastModificationDate,
				lastModificationDate = lastModificationDate
			)
		}
	}

	@RabbitListener(queues = ["orderQueue"])
	fun handleOnOrderPaid(event: OrderPaid) {
		logger.info("Handled event $event")
		with(event) {
			customerOrderFacade.updateCustomerOrderStatus(orderId, CustomerOrderStatus.PAID, lastModificationDate)
		}
	}

	@RabbitListener(queues = ["orderQueue"])
	fun handleOnOrderConfirmed(event: OrderConfirmed) {
		logger.info("Handled event $event")
		with(event) {
			customerOrderFacade.updateCustomerOrderStatus(orderId, CustomerOrderStatus.CONFIRMED, lastModificationDate)
		}
	}

	@RabbitListener(queues = ["orderQueue"])
	fun handleOnOrderPrepared(event: OrderPrepared) {
		logger.info("Handled event $event")
		with(event) {
			customerOrderFacade.updateCustomerOrderStatus(orderId, CustomerOrderStatus.READY, lastModificationDate)
		}
	}

	@RabbitListener(queues = ["orderQueue"])
	fun handleOnOrderDelivered(event: OrderDelivered) {
		logger.info("Handled event $event")
		with(event) {
			customerOrderFacade.updateCustomerOrderStatus(orderId, CustomerOrderStatus.DELIVERED, lastModificationDate)
		}
	}
}