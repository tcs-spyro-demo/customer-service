package com.dbranick.demo.customer.application

internal interface CreateCustomerCommand {
	val firstName: String
	val lastName: String
	val email: String
}