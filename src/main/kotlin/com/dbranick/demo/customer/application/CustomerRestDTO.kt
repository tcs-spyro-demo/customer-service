package com.dbranick.demo.customer.application

internal class CustomerRestDTO(
	val id: String,
	val firstName: String,
	val lastName: String,
	val email: String
)