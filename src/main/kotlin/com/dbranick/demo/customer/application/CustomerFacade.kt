package com.dbranick.demo.customer.application

import com.dbranick.demo.customer.domain.Customer
import com.dbranick.demo.customer.domain.CustomerId
import com.dbranick.demo.customer.domain.CustomerNotFundException
import com.dbranick.demo.customer.domain.CustomerRepository
import org.springframework.stereotype.Component

@Component
internal class CustomerFacade(
	private val customerRepository: CustomerRepository
) {

	fun createCustomer(command: CreateCustomerCommand): String {
		val customer = Customer(command.firstName, command.lastName, command.email)
		customerRepository.save(customer)
		return customer.id.value
	}

	fun getCustomer(id: String): CustomerRestDTO {
		val customerId = CustomerId(id)

		return customerRepository.findCustomer(customerId)
			?.let { CustomerRestDTO(it.id.value, it.firstName, it.lastName, it.email) }
			?: throw CustomerNotFundException(customerId)
	}

}


