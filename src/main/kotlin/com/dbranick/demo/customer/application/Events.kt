package com.dbranick.demo.customer.application

import com.fasterxml.jackson.annotation.JsonSubTypes
import com.fasterxml.jackson.annotation.JsonTypeInfo
import java.time.LocalDateTime

@JsonTypeInfo(use = JsonTypeInfo.Id.NAME, include = JsonTypeInfo.As.PROPERTY, property = "type")
@JsonSubTypes(
	JsonSubTypes.Type(value = OrderCreated::class, name = "OrderCreated"),
	JsonSubTypes.Type(value = OrderPaid::class, name = "OrderPaid"),
	JsonSubTypes.Type(value = OrderConfirmed::class, name = "OrderConfirmed"),
	JsonSubTypes.Type(value = OrderPrepared::class, name = "OrderPrepared"),
	JsonSubTypes.Type(value = OrderDelivered::class, name = "OrderDelivered")
)
internal interface OrderEvent{
	val orderId: String
	val lastModificationDate: LocalDateTime
}

internal data class OrderCreated(
	override val orderId: String,
	override val lastModificationDate: LocalDateTime,
	val customerId: String,
	val number: String
) : OrderEvent

internal data class OrderPaid(
	override val orderId: String,
	override val lastModificationDate: LocalDateTime,
	val paymentMethodId: String
) : OrderEvent

internal data class OrderConfirmed(
	override val orderId: String,
	override val lastModificationDate: LocalDateTime,
) : OrderEvent

internal data class OrderPrepared(
	override val orderId: String,
	override val lastModificationDate: LocalDateTime,
) : OrderEvent

internal data class OrderDelivered(
	override val orderId: String,
	override val lastModificationDate: LocalDateTime,
) : OrderEvent