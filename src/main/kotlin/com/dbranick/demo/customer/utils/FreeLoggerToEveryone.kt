package com.dbranick.demo.customer

import org.slf4j.Logger
import org.slf4j.LoggerFactory
import java.util.concurrent.ConcurrentHashMap
import kotlin.reflect.KClass

public val allLoggersMap: ConcurrentHashMap<KClass<*>, Logger> = ConcurrentHashMap<KClass<*>, Logger>()

public inline val <reified T: Any> KClass<T>.logger: Logger
	get() = allLoggersMap.getOrPut(T::class, { LoggerFactory.getLogger(T::class.java) })

public inline val <reified T: Any> T.logger: Logger
	get() = allLoggersMap.getOrPut(T::class, { LoggerFactory.getLogger(T::class.java) })
